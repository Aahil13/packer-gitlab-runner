# How to bake a Gitlab Runner AMI using Packer

GitLab Runners are applications or services that execute GitLab CI/CD jobs. GitLab CI allows [shared runners](https://docs.gitlab.com/runner/#use-gitlabcom-saas-runners) also known as SaaS runners which are fully managed and hosted on [gitlab.com](https://gitlab.com/)  or [self-managed](https://docs.gitlab.com/runner/#use-self-managed-runners) runners which can be installed and registered on gitlab.com or your server. In this article, you will learn how to use Packer to create an Amazon Machine Image (AMI) that provisions a GitLab runner.

## Prerequisites

This guide takes an intermediate approach to explaining these concepts, but can be easily digested by beginners once they have the following:

- An AWS account. If you don't have one, navigate to the [AWS console](https://aws.amazon.com/resources/create-account/) to create one now.

- A recent [Packer](https://developer.hashicorp.com/packer/downloads) installation.

- A GitLab account. Go to the [official GitLab website](https://gitlab.com/users/sign_up) to create one.

With that, you can proceed with this tutorial.

## Packer configuration

[Packer](https://www.packer.io/) is an open-source tool developed by Hashicorp used to create machine images across several cloud providers such as AWS, GCP, Azure, etc. To write the Packer configuration, you can use JSON or HCL format. In this tutorial, you will use the Hashicorp Configuration Language (HCL) to create the machine image on AWS.

You can find the Packer configuration in the `gitlab-runner-ami.pkr.hcl` file. The following is a breakdown of the configuration file:

1. `packer`: This is the root block of the Packer configuration. It includes a `required_plugins` block specifying the required Amazon plugin and its version. This ensures that the necessary plugins are available during the build process.

   ```JSON
    packer {
      required_plugins {
        amazon = {
           version = ">= 0.0.2"
           source  = "github.com/hashicorp/amazon"
        }
      }
   }
   ```

2. `variable` blocks: These define variables that can be passed to the Packer configuration at runtime. In this case, the variables `instance_type`, `region`, `ssh_username`, and `subnet_id` are defined. These variables allow you to customize the AMI creation process based on your specific requirements.

   ```JSON
    variable "instance_type" {
      type    = string
    }

    variable "region" {
      type    = string
    }

    variable "ssh_username" {
      type    = string
    }

    variable "subnet_id" {
      type    = string
    }
   ```

3. `source` block: This specifies the method for building the AMI. It uses the `amazon-ebs` builder, which creates an AMI in AWS with the Elastic Block Store (EBS). The source is named `ubuntu` and it sets various attributes, such as the AMI name (using `isotime` to generate a timestamp), instance type, region, SSH username, and subnet ID.

   ```JSON
    source "amazon-ebs" "ubuntu" {
        ami_name      = "gitlab-runner-{{isotime \"2006-01-02\"}}"
        instance_type = "${var.instance_type}"
        region        = "${var.region}"
        ssh_username  = "${var.ssh_username}"
        subnet_id     = "${var.subnet_id}"
    }
   ```

4. `source_ami_filter`: This block defines filters to select the base AMI on which the new AMI will be built. It filters the base AMI based on properties like the name, root device type, virtualization type, most recent version, and the owner's AWS account ID.

   ```JSON
   source_ami_filter {
    filters = {
      name                = "ubuntu/images/*hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
   }
   ```

5. `build` block: This defines the build process. In this case, it uses the `source.amazon-ebs.ubuntu` source previously defined.

   ```JSON
    build {
        sources = [
            "source.amazon-ebs.ubuntu"
        ]

        provisioner "file" {
            source      = "./gitlab-runner-provision.sh"
            destination = "/tmp/"
        }

        provisioner "shell" {
            inline = [
            "sudo chmod +x /tmp/gitlab-runner-provision.sh",
            "sudo /tmp/gitlab-runner-provision.sh"
            ]
        }
    }
   ```

    The `build` block includes two provisioners:

    - `provisioner "file"`: This copies the `gitlab-runner-provision.sh` script from the local machine to the `/tmp/` directory on the target instance.

    - `provisioner "shell"`: This executes shell commands on the target instance. It makes the provision script (`gitlab-runner-provision.sh`) executable and runs it using `sudo`.

## The Provision and Registration Scripts

The `gitlab-runner-provision.sh` is a Bash script which performs a series of commands to set up an environment on an Ubuntu-based system. The script installs various dependencies and tools, including Docker, AWS CLI, and GitLab Runner. It also adds repositories for Docker and GitLab Runner, allowing these tools to be installed and used on the system. This script will automate the provisioning of GitLab runner on the AMI.

The `gitlab-runner-register.sh` is a Bash script that registers a GitLab Runner on a specific GitLab instance. The script takes two command-line arguments: an **environment tag** - the tag in the `gitlab-ci.yml` file and a **GitLab registration token** - which can be found on the GitLab console.

To get the GitLab registration token, follow the steps below:

- On your GitLab console, Settings > CI/CD > Runners (Click on Expand).

    ![GitLab CI console](images/Gitlab-CI-console.jpeg)

- On the **New project runner** button, click on the three-dotted menu and copy the registration token.

    ![GitLab CI registration token](images/Gitlab-CI-Registration-token.jpeg)

## How to use this project

The following steps will help you use this project:

1. Clone this repository:

   ```shell
   git clone https://gitlab.com/Aahil13/packer-gitlab-runner.git
   ```

2. Create your `variables.pkvars.hcl` file for your variables using the `variables.pkvars.hcl.example` template:

   ```JSON
    instance_type = <value>
    region        = <value>
    ssh_username  = <value>
    subnet_id     = <value>
   ```

3. Change the directory to the project and run the following commands in sequence:

   ```shell
    cd [project name]
    packer init .             # Initializes Packer and downloads all requirements
    packer fmt .              # Checks Packer configuration for formatting issues
    packer validate .         # Validates the Packer configuration
    packer build --var-file= variables.pkvars.hcl gitlab-runner-ami.pkr.hcl # Builds the AMI using the specified variables.
   ```

    **Note**: To use this configuration, you need to authenticate with AWS. You can use the [official Packer documentation](https://developer.hashicorp.com/packer/tutorials/aws-get-started/aws-get-started-build-image#authenticate-to-aws) to do this.

4. Log in to your AWS account to view the AMI. Launch an EC2 instance with this AMI and in the user data section, copy the `gitlab-runner-register.sh` script and paste it. Make sure to specify the `TAG` and `TOKEN` variables with the environment tag and GitLab registration token.

   ```shell
    #!/usr/bin/env bash

     sudo gitlab-runner register \
      --non-interactive \
      --url "https://gitlab.com/" \
      --registration-token "${TOKEN}" \
      --executor "docker" \
      --docker-image alpine:latest \
      --docker-privileged \
      --description "${TAG}-gitlab-runner" \
      --tag-list "${TAG},all" \
      --run-untagged="false" \
      --locked="false" \
      --access-level="not_protected"
   ```

    This will register the GitLab runner while the EC2 instance is created.

    To see the runner, navigate to GitLab console > settings > CI/CD > Runners > Assigned project runners.

    ![GitLab runner confirmation](images/Gitlab-runner-confirmation.jpeg)

5. Create a sample `.gitlab-ci.yml` file to test the GitLab runner. Make sure to specify the same tag as used while registering the GitLab runner.

   ```yaml
   stages:
     - build

   job:
     stage: build
     tags: 
       - dev
     script:
       - echo "Hello world!"
   ```

    In this project, you can use the `dev` environment. Feel free to change this to any environment such as `test`, etc. GitLab Runners executes only jobs with the same tag.

## Credits

- [Aahil](https://www.linkedin.com/in/prince-onyeanuna-607352246/)
- [Fransis Iduh](https://www.linkedin.com/in/osemhenkan-iduh)

## Conclusion

This is a simple and easy way of creating a GitLab runner AMI using Packer. If you have any contributions, feel free to fork this repository and make a pull request. Star this repository and share it with your friends!
