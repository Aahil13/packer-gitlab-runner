packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "instance_type" {
  type    = string
}

variable "region" {
  type    = string
}

variable "ssh_username" {
  type    = string
}

variable "subnet_id" {
  type    = string
}


source "amazon-ebs" "ubuntu" {
  ami_name      = "gitlab-runner-{{isotime \"2006-01-02\"}}"
  instance_type = "${var.instance_type}"
  region        = "${var.region}"
  ssh_username  = "${var.ssh_username}"
  subnet_id     = "${var.subnet_id}"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
}

build {
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "file" {
    source      = "./gitlab-runner-provision.sh"
    destination = "/tmp/"
  }

  provisioner "shell" {
    inline = [
      "sudo chmod +x /tmp/gitlab-runner-provision.sh",
      "sudo /tmp/gitlab-runner-provision.sh"
    ]
  }
}