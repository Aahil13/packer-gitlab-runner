#!/usr/bin/env bash

#Wait for cloud-init to finish (if running on a cloud instance)
#This ensures that /etc/apt/sources.list is updated before proceeding
if command -v cloud-init > /dev/null 2>&1; then
  echo "Waiting for cloud-init to complete..."
  cloud-init status --wait
fi

# Update the package index
sudo apt-get update

# Install required packages
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    awscli

# Get the Docker repo GPG key and add Docker Ubuntu repository
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list

# Add the Gitlab repo
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# Install Docker CE and the Gitlab runner
sudo -E apt-get -y install docker-ce docker-ce-cli containerd.io gitlab-runner

# Start and enable the Docker service
sudo systemctl enable docker
sudo systemctl start docker

# Add user to the docker group to avoid using sudo for docker commands
sudo usermod -aG docker $(whoami)
