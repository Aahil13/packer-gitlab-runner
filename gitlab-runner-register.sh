#!/usr/bin/env bash

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "${TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --docker-privileged \
  --description "${TAG}-gitlab-runner" \
  --tag-list "${TAG},all" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
